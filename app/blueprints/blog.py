#! /usr/bin/env python3
# coding: utf-8


from flask import Blueprint

blog_bp = Blueprint('blog', __name__)


@blog_bp.route('/')
@blog_bp.route('/index')
def index():
    return "hello world!!!"
