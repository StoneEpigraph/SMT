#! /usr/bin/env python3
# coding: utf-8

import os
import json
from datetime import datetime


class ResponseUtil:
    @staticmethod
    def response_msg(code=0, msg="成功", data={}, debugMsg=""):
        response_info = {}
        if data:
            response_info['data'] = data
        if os.getenv('FLASK_DEBUG') and debugMsg:
            msg = msg + "(" + debugMsg + ")"
        response_info['code'] = code
        response_info['msg'] = msg
        return json.dumps(response_info, cls=DateEncoder)


class DateEncoder(json.JSONEncoder ):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.__str__()
        return json.JSONEncoder.default(self, obj)