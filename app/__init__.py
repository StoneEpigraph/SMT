import os

from flask import Flask
from extensions import mail, db
from app.blueprints.blog import blog_bp
from app.blueprints.mail import mail_bp

from settings import config


def create_app(config_name=None):
    if not config_name:
        config_name = os.getenv("FLASK_CONFIG", "development")
        print(config_name)

    app = Flask('SMT')
    app.config.from_object(config[config_name])

    register_extensions(app)
    register_blueprint(app)
    return app


def register_blueprint(app):
    app.register_blueprint(blog_bp)
    app.register_blueprint(mail_bp, url_prefix='/mail')


def register_extensions(app):
    db.init_app(app)
    mail.init_app(app)