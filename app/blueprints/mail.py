#! /usr/bin/env python3
# coding: utf-8

from flask import Blueprint
from flask_mail import Message
from extensions import mail
from flask import request

from utils.ResponseUtil import ResponseUtil


mail_bp = Blueprint('mail', __name__)


@mail_bp.route("/send_mail")
def send_mail():
    subject = request.args.get('subject', '').encode('utf-8')
    to = request.args.get('to', '').encode('utf-8')
    body = request.args.get('body', '').encode('utf-8')
    recipients = []
    if not subject:
        subject = "no Subject"
    if not to:
        recipients = ["stoneepigraph@163.com",]
    else:
        if isinstance(to, (str)):
            recipients = [to]
        elif isinstance(to, (list)):
            recipients = to
        elif isinstance(to, (tuple)):
            recipients = list(to)

    if not body:
        body = "No Body"

    message = Message(subject, recipients=recipients, body=body)
    mail.send(message)

    return ResponseUtil.response_msg()
