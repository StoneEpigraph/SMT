#! /usr/bin/env python3
# coding: utf-8

import os
import sys


basedir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))


class BaseConfig:
    SECRET_KEY = os.getenv('SECRET_key', 'DEV_KEY')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(BaseConfig):
    MAIL_SERVER = "smtp.sina.com"
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = "stoneepigraph@sina.com"
    MAIL_PASSWORD = "qfhwrepwq@521"
    MAIL_DEFAULT_SENDER = ("Stone", "stoneepigraph@sina.com")


class TestingConfig(BaseConfig):
    pass


class ProductionConfig(BaseConfig):
    pass


config = {
    "development": DevelopmentConfig,
    "testing": TestingConfig,
    "production": ProductionConfig
}